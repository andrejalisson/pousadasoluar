<?php

use Illuminate\Support\Facades\Route;

//SITE
Route::get('/', 'App\Http\Controllers\SiteController@home');
//ACESSO
Route::get('/Login', 'App\Http\Controllers\AcessoController@login');
Route::get('/Logout', 'App\Http\Controllers\AcessoController@logout');
Route::get('/Cadastro', 'App\Http\Controllers\AcessoController@cadastro');
Route::get('/Recupera', 'App\Http\Controllers\AcessoController@recupera');
Route::get('/Recuperar/{token}', 'AcessoController@verificaToken');
Route::post('/Recuperar', 'AcessoController@novasenha');
Route::post('/Login', 'App\Http\Controllers\AcessoController@verifica');
Route::post('/Cadastro', 'App\Http\Controllers\AcessoController@cadastroAdd');
Route::post('/Recupera', 'App\Http\Controllers\AcessoController@recuperaAdd');
//DASHBOARD
Route::get('/Dashboard', 'App\Http\Controllers\DashBoardsController@reservas');
//USUARIO
Route::get('/Usuarios', 'App\Http\Controllers\UsuariosController@lista');
Route::post('/todasUsuarios', 'App\Http\Controllers\UsuariosController@todasUsuarios')->name('todasUsuarios');
Route::post('/excluiUsuario', 'App\Http\Controllers\UsuariosController@excluiUsuario')->name('excluiUsuario');
Route::get('/Usuarios/Adicionar', 'App\Http\Controllers\UsuariosController@add');
Route::post('/Usuarios/Adicionar', 'App\Http\Controllers\UsuariosController@addPost');
Route::get('/Usuarios/Editar/{id}', 'App\Http\Controllers\UsuariosController@editar');
Route::post('/Usuarios/Editar/{id}', 'App\Http\Controllers\UsuariosController@edtPost');
//ACOMODAÇÕES
Route::get('/Acomodacoes', 'App\Http\Controllers\AcomodacaoController@lista');
Route::get('/Acomodacoes/Adicionar', 'App\Http\Controllers\AcomodacaoController@add');
Route::post('/Acomodacoes/Adicionar', 'App\Http\Controllers\AcomodacaoController@addPost');
Route::post('/Acomodacoes/Imagens','App\Http\Controllers\AcomodacaoController@imagens');
