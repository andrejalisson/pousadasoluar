@extends('templates.admin')

@section('css')
<link href="/admin/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="/admin/css/plugins/select2/select2-bootstrap4.min.css" rel="stylesheet">
<link href="/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title  back-change">
                    <h5>{{$title}}</h5>
                </div>
                <div class="ibox-content">
                    <form role="form" method="POST" action="/Usuarios/Editar/{{$usuario->id}}">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Nome</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" required name="nome" value="{{$usuario->nome}}">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Email</label>
                                        <div class="input-group">
                                            <input type="email" class="form-control" required name="email" value="{{$usuario->email}}">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-2">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Status</label>
                                        <div class="input-group">
                                            @php
                                                if ($usuario->status == 1) {
                                                    $status = "checked";
                                                }else{
                                                    $status = "";
                                                }
                                            @endphp
                                            <input type="checkbox" name="status" class="js-switch" {{$status}} />
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-3">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">WhatsApp</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" data-mask="(00)0 0000-0000" required name="whatsapp" value="{{$usuario->whatsapp}}">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-3">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Tipo de acesso</label>
                                        <div class="input-group">
                                            <select class="select2_demo_1 form-control" name="tipo">
                                                @if ($usuario->tipo == 1)
                                                    <option selected value="1">Administrador</option>
                                                    <option value="2">Vendedor</option>
                                                    <option value="3">Cliente</option>
                                                @endif
                                                @if ($usuario->tipo == 2)
                                                    <option value="1">Administrador</option>
                                                    <option selected value="2">Vendedor</option>
                                                    <option value="3">Cliente</option>
                                                @endif
                                                @if ($usuario->tipo == 3)
                                                    <option value="1">Administrador</option>
                                                    <option value="2">Vendedor</option>
                                                    <option selected value="3">Cliente</option>
                                                @endif
                                    
                                            </select>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-3">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Senha <Small>(Caso não queira alterar, deixar em branco.)</Small></label>
                                        <div class="input-group">
                                            <input type="password" id="senha" class="form-control" name="senha1">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-3">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Repetir a Senha <Small>(Caso não queira alterar, deixar em branco.)</Small></label>
                                        <div class="input-group">
                                            <input type="password" id="senha" class="form-control" name="senha2">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div  class="col-md-12">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary btn-sm" type="submit">Editar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="/admin/js/plugins/select2/select2.full.min.js"></script>
<script src="/admin/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="/admin/js/plugins/switchery/switchery.js"></script>
<script src="/admin/js/plugins/jqueryMask/jquery.mask.min.js"></script>
@endsection

@section('script')
<script>
    $(document).ready(function(){

        var elem = document.querySelector('.js-switch');
        var switchery = new Switchery(elem, { color: '#1AB394' });

    });

</script>
@endsection
