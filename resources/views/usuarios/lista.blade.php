@extends('templates.admin')

@section('css')
<link href="/admin/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="/admin/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox ">
                        <div class="ibox-title">
                            <div class="ibox-tools">
                                <button class="btn btn-primary" onclick="location.href='/Usuarios/Adicionar'"  type="button"><i class="fa fa-plus"></i> Adicionar</button>
                            </div>
                        </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover dataTables-example" id="usuarios">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nome</th>
                                                <th>Email</th>
                                                <th>WhatsApp</th>
                                                <th>Tipo</th>
                                                <th>Status</th>
                                                <th>Opções</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('js')
<script src="/admin/js/plugins/dataTables/datatables.min.js"></script>
<script src="/admin/js/plugins/sweetalert/sweetalert.min.js"></script>
@endsection

@section('script')
<script>

$(function () {
    $(document).ready(function () {

        
        $(document).on('click', '.excluirusuario', function(){
            var usuario = $(this).data('usuario');
            swal({
                title: "Tem certeza?",
                text: "Você não será capaz de recuperar este usuário futuramente!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, exclua!",
                cancelButtonText: "Não, cancele!",
                closeOnConfirm: false,
                closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
                    dados = {
                        id : usuario,
                        _token : "{{csrf_token()}}"
                    };

                    $.ajax({
                        url            : "{{url('excluiUsuario')}}",
                        type           : "POST",
                        dataType       : 'JSON',
                        data           : dados,
                        success: function () {
                            var table = $('#usuarios').DataTable();
                            table.ajax.reload();
                            swal("Excluído!", "O usuário foi excluído com sucesso", "success");
                        }, error: function(){
                            swal("Erro!", "Não foi possível remover o usuário.", "error");
                        }
                    });
                } else {
                    swal("Cancelado", "Ufa, essa foi por pouco.", "error");
                }
            });
        });

        
        


        $('#usuarios').dataTable({
            "processing": true,
            "order": [[ 1, "asc" ]],
            "serverSide": true,
            "oLanguage": {
            "sLengthMenu": "Mostrar _MENU_ registros por página",
            "sZeroRecords": "Nenhum registro encontrado",
            "sInfo": "Mostrando _END_ de _TOTAL_ registro(s)",
            "sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
            "sInfoFiltered": "(filtrado de _MAX_ registros)",
            "sSearch": "Pesquisar: ",
            "oPaginate": {
                "sFirst": "Início",
                "sPrevious": "Anterior",
                "sNext": "Próximo",
                "sLast": "Último"
                }
            },
            "ajax":{
                    "url": "{{ url('todasUsuarios') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{
                        _token: "{{csrf_token()}}"

                    }
                },
            "columns": [
                { "data": "id" },
                { "data": "nome" },
                { "data": "email" },
                { "data": "whatsapp" },
                { "data": "tipo" },
                { "data": "status" },
                { "data": "opcoes" }
        ]
        });
    });
});
        

</script>
@endsection
