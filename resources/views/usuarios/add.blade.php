@extends('templates.admin')

@section('css')
<link href="/admin/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="/admin/css/plugins/select2/select2-bootstrap4.min.css" rel="stylesheet">
<link href="/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title  back-change">
                    <h5>{{$title}}</h5>
                </div>
                <div class="ibox-content">
                    <form role="form" method="POST" action="/Usuarios/Adicionar">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Nome</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" required name="nome" placeholder="Nome do Usuário">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Email</label>
                                        <div class="input-group">
                                            <input type="email" class="form-control" required name="email" placeholder="Email">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-2">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Status</label>
                                        <div class="input-group">
                                            <input type="checkbox" name="status" class="js-switch" checked />
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-3">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">WhatsApp</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" data-mask="(00)0 0000-0000" required name="whatsapp" placeholder="(88) 9 8888-8888">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-3">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Tipo de acesso</label>
                                        <div class="input-group">
                                            <select class="select2_demo_1 form-control" name="tipo">
                                                <option value="1">Administrador</option>
                                                <option selected value="2">Vendedor</option>
                                                <option value="3">Cliente</option>
                                            </select>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-3">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Senha <Small>Padrão (123mudar)</Small></label>
                                        <div class="input-group">
                                            <input type="password" id="senha" required class="form-control" name="senha1" value="123mudar">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-3">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Repetir a Senha <Small>Padrão (123mudar)</Small></label>
                                        <div class="input-group">
                                            <input type="password" id="senha" required class="form-control" name="senha2" value="123mudar">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div  class="col-md-12">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary btn-sm" type="submit">Adicionar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="/admin/js/plugins/select2/select2.full.min.js"></script>
<script src="/admin/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="/admin/js/plugins/switchery/switchery.js"></script>
<script src="/admin/js/plugins/jqueryMask/jquery.mask.min.js"></script>
@endsection

@section('script')
<script>
    $(document).ready(function(){

        var elem = document.querySelector('.js-switch');
        var switchery = new Switchery(elem, { color: '#1AB394' });

    });

</script>
@endsection
