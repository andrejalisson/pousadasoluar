<!DOCTYPE html>
<html lang="pt-BR">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ $title }} | Pousada Soluar</title>

    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="/admin/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/iziToast/dist/css/iziToast.min.css" rel="stylesheet" >
    @yield('css')
    <link href="/admin/css/animate.css" rel="stylesheet">
    <link href="/admin/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    @yield('corpo')

    

</body>
@yield('js')
<script src="/admin/js/jquery-3.1.1.min.js"></script>
<script src="/admin/js/popper.min.js"></script>
<script src="/admin/js/bootstrap.js"></script>
<script src="/iziToast/dist/js/iziToast.min.js"></script>
<script>
    @yield('script')
</script>
<script>
    $(document).ready(function(){
        @if (session('sucesso'))
            iziToast.success({
                title: ':)',
                transitionIn: 'bounceInLeft',
                position: 'topRight',
                message: "{{session('sucesso')}}",
            });
        @endif
        @if (session('alerta'))
            iziToast.warning({
                title: 'O.o',
                transitionIn: 'bounceInLeft',
                position: 'topLeft',
                message: "{{session('alerta')}}",
            });
        @endif
        @if (session('erro'))
            iziToast.error({
                title: ':X',
                transitionIn: 'bounceInLeft',
                position: 'topRight',
                message: "{{session('erro')}}",
            });
        @endif

    });

</script>
</html>
