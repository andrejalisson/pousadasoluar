<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{$title}} | Pousada Soluar</title>
        <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
        <link href="/admin/font-awesome/css/all.css" rel="stylesheet">
        @yield('css')
        <link href="/admin/css/animate.css" rel="stylesheet">
        <link href="/admin/css/style.css" rel="stylesheet">
    </head>
    <body class="md-skin pace-done">
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element">
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    @php
                                        $string = session('nome');
                                        $partes = explode(' ', $string);
                                        $nome = $partes[0]." ".array_pop($partes);
                                    @endphp
                                    <span class="block m-t-xs font-bold">{{$nome}}</span>
                                    @php
                                        switch (session('tipo')) {
                                            case 1:
                                                $tipo = "Administrador";
                                                break;
                                            case 2:
                                                $tipo = "Vendedor";
                                                break;
                                            case 3:
                                                $tipo = "Cliente";
                                                break;
                                            
                                            default:
                                                # code...
                                                break;
                                        }
                                    @endphp
                                    <span class="text-muted text-xs block">{{$tipo}}<b class="caret"></b></span>
                                </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li><a class="dropdown-item" href="#">Perfil</a></li>
                                    <li class="dropdown-divider"></li>
                                    <li><a class="dropdown-item" href="/Logout">Sair</a></li>
                                </ul>
                            </div>
                            <div class="logo-element">
                                SL
                            </div>
                        </li>
                        <li class="active">
                            <a href="#"><i class="fas fa-chart-bar"></i> <span class="nav-label">Dashboards</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li class="active"><a href="/Dashboard">Reservas</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fas fa-bed"></i> <span class="nav-label">Acomodações</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="/Acomodacoes">Lista de Acomodações</a></li>
                                <li><a href="/Acomodacoes/Adicionar">Lista de Acomodações</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fas fa-user-tie"></i> <span class="nav-label">Usuários</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="/Usuarios">Lista de Usuários</a></li>
                                <li><a href="/Usuarios/Adicionar">Adicionar Usuários</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fas fa-donate"></i> <span class="nav-label">Financeiro</span> <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="/Usuarios">Caixa</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                            <form role="search" class="navbar-form-custom" action="#">
                                <div class="form-group">
                                    <input type="text" placeholder="Pesquisar por Clientes" class="form-control" name="top-search" id="top-search">
                                </div>
                            </form>
                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <span class="m-r-sm text-muted welcome-message">Bem vindo ao sistema Soluar</span>
                            </li>
                            <li>
                                <a href="/Logout">
                                    <i class="fa fa-sign-out"></i> Sair
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                @yield('corpo')
                <div class="footer">
                    <div class="float-right">
                        Suporte: <strong>85 9 8596-5372</strong>
                    </div>
                <div>
                    <strong>André Jálisson</strong> Web Developer &copy; 2011-{{date('Y')}}
                </div>
            </div>
        </div>        
        <!-- Mainly scripts -->
        <script src="/admin/js/jquery-3.1.1.min.js"></script>
        <script src="/admin/js/popper.min.js"></script>
        <script src="/admin/js/bootstrap.js"></script>
        <script src="/admin/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="/admin/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="/admin/js/inspinia.js"></script>
        <script src="/admin/js/plugins/pace/pace.min.js"></script>
        @yield('js')
        <!-- jQuery UI -->
        <script src="/admin/js/plugins/jquery-ui/jquery-ui.min.js"></script>
        @yield('script')
    </body>
</html>