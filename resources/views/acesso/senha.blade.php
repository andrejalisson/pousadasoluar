@extends('templates.acesso')

@section('css')

@endsection

@section('corpo')
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">SoLuar</h1>
        </div>
        <h3>Crie uma nova senha.</h3>
        <p>Lembre-se: Não divida sua senha com ninguém, seu acesso é monitorado e tudo realizado com seu login e de sua responsabilidade.</p>
        <form class="m-t" role="form" method="POST" action="/Recuperar">
            {!! csrf_field() !!}
            <input type="hidden" name="token" value="{{$token}}">
            <div class="form-group">
                <input type="password" class="form-control" name="senha" placeholder="Nova Senha" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Salvar</button>
        </form>
        <p class="m-t"> <small>Web Developer: André Jálisson ♥</small> </p>
    </div>
</div>
@endsection

@section('js')

@endsection

@section('script')

@endsection

