@extends('templates.acesso')

@section('css')

@endsection

@section('corpo')
<div class="middle-box text-center loginscreen   animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">SoLuar</h1>
        </div>
        <h3>Sistema Administrativo SoLuar</h3>
        <p>Realize seu cadastro e espere os administradores aprovarem seu cadastro.</p>
        <form class="m-t" role="form" method="POST" action="/Cadastro">
            {!! csrf_field() !!}
            <div class="form-group">
                <input type="text" class="form-control" autofocus placeholder="Nome" name="nome" required="">
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="E-mail" name="email" required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Senha" name="senha" required="">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="WhatsApp" name="whatsapp" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Cadastrar</button>

            <p class="text-muted text-center"><small>Já Tem Conta?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="/Login">Efetue o Login</a>
        </form>
        <p class="m-t"> <small>Web Developer: André Jálisson ♥</small> </p>
    </div>
</div>
@endsection

@section('js')

@endsection

@section('script')

@endsection

