@extends('templates.acesso')

@section('css')

@endsection

@section('corpo')
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">SoLuar</h1>
        </div>
        <h3>Sistema Administrativo SoLuar</h3>
        <p>Informe suas credenciais para continuar...</p>
        <form class="m-t" role="form" method="POST" action="/Login">
            {!! csrf_field() !!}
            <div class="form-group">
                <input type="email" class="form-control" placeholder="E-mail" name="usuario"required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Senha" name="senha" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Entrar</button>

            <a href="/Recupera"><small>Esqueceu a Senha?</small></a>
            <p class="text-muted text-center"><small>Ainda não tem conta?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="/Cadastro">Criar Sua Conta.</a>
        </form>
        <p class="m-t"> <small>Web Developer: André Jálisson ♥</small> </p>
    </div>
</div>
@endsection

@section('js')

@endsection

@section('script')

@endsection

