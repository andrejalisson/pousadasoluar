@extends('templates.acesso')

@section('css')

@endsection

@section('corpo')
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">SoLuar</h1>
        </div>
        <h3>Sistema Administrativo SoLuar</h3>
        <p>Esqueceu a senha?</p>
        <p>Um email será enviado com as instruções para recuperação.</p>
        <form class="m-t" role="form" method="POST" action="/Recupera">
            {!! csrf_field() !!}
            <div class="form-group">
                <input type="email" class="form-control" placeholder="E-mail" name="email" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Recuperar</button>
        </form>
        <p class="m-t"> <small>Web Developer: André Jálisson ♥</small> </p>
    </div>
</div>
@endsection

@section('js')

@endsection

@section('script')

@endsection

