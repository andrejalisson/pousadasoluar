@extends('templates.admin')

@section('css')

@endsection

@section('corpo')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{{$title}}</h2>
    </div>
    <div class="col-lg-2">
        <div class="ibox-tools">
            <button class="btn btn-primary" onclick="location.href='/Acomodacoes/Adicionar'"  type="button"><i class="fa fa-plus"></i> Adicionar</button>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox">
                <div class="ibox-title">
                    <span class="label label-primary float-right">Mostra no Site</span>
                    <h5>Quarto Standard</h5>
                </div>
                <div class="ibox-content">
                    <div class="team-members">
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a1.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a2.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a3.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a5.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a6.jpg"></a>
                    </div>
                    <h4>101</h4>
                    <p>
                        Aqui fica uma breve descrição da acomodação para aparecer no site.
                    </p>
                    <div>
                        <span>Ocupação no mês</span>
                        <div class="stat-percent">48%</div>
                        <div class="progress progress-mini">
                            <div style="width: 48%;" class="progress-bar"></div>
                        </div>
                    </div>
                    <div class="row  m-t-sm">
                        <div class="col-sm-4">
                            <div class="font-bold">Reservas do mês</div>
                            12
                        </div>
                        <div class="col-sm-4">
                            <div class="font-bold">Cancelamentos</div>
                            4
                        </div>
                        <div class="col-sm-4 text-right">
                            <div class="font-bold">Renda bruta.</div>
                            R$1.200,90 <i class="fa fa-level-up text-navy"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox">
                <div class="ibox-title">
                    <span class="label label-primary float-right">Mostra no Site</span>
                    <h5>Quarto Master</h5>
                </div>
                <div class="ibox-content">
                    <div class="team-members">
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a1.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a2.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a3.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a5.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a6.jpg"></a>
                    </div>
                    <h4>201</h4>
                    <p>
                        Aqui fica uma breve descrição da acomodação para aparecer no site.
                    </p>
                    <div>
                        <span>Ocupação no mês</span>
                        <div class="stat-percent">48%</div>
                        <div class="progress progress-mini">
                            <div style="width: 48%;" class="progress-bar"></div>
                        </div>
                    </div>
                    <div class="row  m-t-sm">
                        <div class="col-sm-4">
                            <div class="font-bold">Reservas do mês</div>
                            12
                        </div>
                        <div class="col-sm-4">
                            <div class="font-bold">Cancelamentos</div>
                            4th
                        </div>
                        <div class="col-sm-4 text-right">
                            <div class="font-bold">Renda bruta.</div>
                            R$1.200,90 <i class="fa fa-level-up text-navy"></i>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox">
                <div class="ibox-title">
                    <span class="label label-primary float-right">Mostra no Site</span>
                    <h5>Quarto Deluxe</h5>
                </div>
                <div class="ibox-content">
                    <div class="team-members">
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a1.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a2.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a3.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a5.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a6.jpg"></a>
                    </div>
                    <h4>301</h4>
                    <p>
                        Aqui fica uma breve descrição da acomodação para aparecer no site.
                    </p>
                    <div>
                        <span>Ocupação no mês</span>
                        <div class="stat-percent">48%</div>
                        <div class="progress progress-mini">
                            <div style="width: 48%;" class="progress-bar"></div>
                        </div>
                    </div>
                    <div class="row  m-t-sm">
                        <div class="col-sm-4">
                            <div class="font-bold">Reservas do mês</div>
                            12
                        </div>
                        <div class="col-sm-4">
                            <div class="font-bold">Cancelamentos</div>
                            4th
                        </div>
                        <div class="col-sm-4 text-right">
                            <div class="font-bold">Renda bruta.</div>
                            R$1.200,90 <i class="fa fa-level-up text-navy"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Quarto Standard</h5>
                </div>
                <div class="ibox-content">
                    <div class="team-members">
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a1.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a2.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a3.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a5.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a6.jpg"></a>
                    </div>
                    <h4>102</h4>
                    <p>
                        Aqui fica uma breve descrição da acomodação para aparecer no site.
                    </p>
                    <div>
                        <span>Ocupação no mês</span>
                        <div class="stat-percent">48%</div>
                        <div class="progress progress-mini">
                            <div style="width: 48%;" class="progress-bar"></div>
                        </div>
                    </div>
                    <div class="row  m-t-sm">
                        <div class="col-sm-4">
                            <div class="font-bold">Reservas do mês</div>
                            12
                        </div>
                        <div class="col-sm-4">
                            <div class="font-bold">Cancelamentos</div>
                            4
                        </div>
                        <div class="col-sm-4 text-right">
                            <div class="font-bold">Renda bruta.</div>
                            R$1.200,90 <i class="fa fa-level-up text-navy"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Quarto Master</h5>
                </div>
                <div class="ibox-content">
                    <div class="team-members">
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a1.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a2.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a3.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a5.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a6.jpg"></a>
                    </div>
                    <h4>202</h4>
                    <p>
                        Aqui fica uma breve descrição da acomodação para aparecer no site.
                    </p>
                    <div>
                        <span>Ocupação no mês</span>
                        <div class="stat-percent">48%</div>
                        <div class="progress progress-mini">
                            <div style="width: 48%;" class="progress-bar"></div>
                        </div>
                    </div>
                    <div class="row  m-t-sm">
                        <div class="col-sm-4">
                            <div class="font-bold">Reservas do mês</div>
                            12
                        </div>
                        <div class="col-sm-4">
                            <div class="font-bold">Cancelamentos</div>
                            4th
                        </div>
                        <div class="col-sm-4 text-right">
                            <div class="font-bold">Renda bruta.</div>
                            R$1.200,90 <i class="fa fa-level-up text-navy"></i>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Quarto Deluxe</h5>
                </div>
                <div class="ibox-content">
                    <div class="team-members">
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a1.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a2.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a3.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a5.jpg"></a>
                        <a href="#"><img alt="member" class="rounded-circle" src="/admin/img/a6.jpg"></a>
                    </div>
                    <h4>302</h4>
                    <p>
                        Aqui fica uma breve descrição da acomodação para aparecer no site.
                    </p>
                    <div>
                        <span>Ocupação no mês</span>
                        <div class="stat-percent">48%</div>
                        <div class="progress progress-mini">
                            <div style="width: 48%;" class="progress-bar"></div>
                        </div>
                    </div>
                    <div class="row  m-t-sm">
                        <div class="col-sm-4">
                            <div class="font-bold">Reservas do mês</div>
                            12
                        </div>
                        <div class="col-sm-4">
                            <div class="font-bold">Cancelamentos</div>
                            4th
                        </div>
                        <div class="col-sm-4 text-right">
                            <div class="font-bold">Renda bruta.</div>
                            R$1.200,90 <i class="fa fa-level-up text-navy"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

@endsection

@section('script')

@endsection
