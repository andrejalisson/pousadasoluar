@extends('templates.admin')

@section('css')
<link href="/admin/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="/admin/css/plugins/select2/select2-bootstrap4.min.css" rel="stylesheet">
<link href="/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
<link href="/admin/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
<link href="/admin/css/plugins/dropzone/basic.css" rel="stylesheet">
<link href="/admin/css/plugins/dropzone/dropzone.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title  back-change">
                    <h5>{{$title}}</h5>
                </div>
                <div class="ibox-content">
                    <form action="{{url('/Acomodacoes/Imagens')}}" class="dropzone" method="POST" id="dropzoneForm" enctype="multipart/form-data" >
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                        {!! csrf_field() !!}
                    </form>
                    <form role="form" method="POST" action="/Acomodacoes/Adicionar">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-5">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Título</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" required name="titulo" placeholder="Título da Acomodação">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-md-3">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Valor R$</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control money" required name="valor" placeholder="0,00">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-2">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Site?</label>
                                        <div class="input-group">
                                            <input type="checkbox" id="site" name="site" class="js-switch" />
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-2">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Status</label>
                                        <div class="input-group">
                                            <input type="checkbox" id="status" name="status" class="js-switch" checked />
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Descrição</label>
                                        <div class="input-group">
                                            <textarea name="descricao" cols="500" rows="5"></textarea>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Atributos</label>
                                        <div class="input-group">
                                            <input class="tagsinput form-control" name="atributos" type="text" value="WiFi,Armários,Café da manhã incluso,Lavanderia Disponível,Acesso 24 horas por dia"/>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div  class="col-md-12">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary btn-sm" type="submit">Adicionar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="/admin/js/plugins/select2/select2.full.min.js"></script>
<script src="/admin/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="/admin/js/plugins/switchery/switchery.js"></script>
<script src="/admin/js/plugins/jqueryMask/jquery.mask.min.js"></script>
<script src="/admin/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="/admin/js/plugins/dropzone/dropzone.js"></script>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
            event.preventDefault();
            return false;
            }
        });
        Dropzone.options.dropzoneForm = {
            paramName: 'file',
            maxFilesize: 20, // MB
            renameFile: function(file) {
                var dt = new Date();
                var time = dt.getTime();
               return time+file.name;
            },
            dictDefaultMessage: "Arraste as imagens para cá!",
            acceptedFiles: ".jpeg,.jpg,.png",
            addRemoveLinks: true,
            timeout: 5000,
            success: function(file, response){
                console.log(response);
            },
            error: function(file, response){
               return false;
            }
        };

        $('.tagsinput').tagsinput({
            tagClass: 'label label-primary'
        });
        var elem = document.querySelector('#site');
        var switchery = new Switchery(elem, { color: '#1AB394' });
        var elem1 = document.querySelector('#status');
        var switchery = new Switchery(elem1, { color: '#1AB394' });
        $('.money').mask('#.##0,00', {reverse: true});

    });

</script>
@endsection
