@extends('templates.admin')

@section('css')

@endsection

@section('corpo')
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-3">
            <div class="ibox ">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        @php
                            setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                            date_default_timezone_set('America/Fortaleza');
                        @endphp
                        <span class="label label-success float-right">Hoje</span>
                    </div>
                    <h5>Reservas</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">2</h1>
                    <div class="stat-percent font-bold text-success">25% <i class="fa fa-bolt"></i></div>
                    <small>Ocupação</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox ">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        <span class="label label-info float-right">Semana Atual</span>
                    </div>
                    <h5>Reservas</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">9</h1>
                    <div class="stat-percent font-bold text-info">20% <i class="fas fa-level-up-alt"></i></div>
                    <small>Comparação com Semana Passada</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox ">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        <span class="label label-primary float-right">{{strftime('%B', strtotime('today'))}}</span>
                    </div>
                    <h5>Reservas</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">20</h1>
                    <div class="stat-percent font-bold text-navy">44% <i class="fas fa-level-up-alt"></i></div>
                    <small>Comparação com Mês Passado</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox ">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        <span class="label label-danger float-right">{{strftime('%Y', strtotime('today'))}}</span>
                    </div>
                    <h5>Reservas</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">659</h1>
                    <div class="stat-percent font-bold text-danger">38% <i class="fas fa-level-down-alt"></i></div>
                    <small>Comparação com Ano Passado</small>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

@endsection

@section('script')

@endsection
