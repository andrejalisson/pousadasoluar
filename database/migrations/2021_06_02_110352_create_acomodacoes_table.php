<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcomodacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('acomodacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 50);
            $table->float('valor')->default(0.00);
            $table->text('atributos');
            $table->boolean('site')->default(false);
            $table->boolean('status')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acomodacoes');
    }
}
