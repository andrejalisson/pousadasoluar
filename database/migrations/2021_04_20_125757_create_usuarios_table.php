<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 100);
            $table->string('senha', 72)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('whatsapp', 11)->nullable();
            $table->dateTime('criacao')->nullable();
            $table->integer('tentativas')->unsigned()->nullable()->default(0);
            $table->integer('tipo')->unsigned()->nullable()->default(3);
            $table->boolean('status')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
