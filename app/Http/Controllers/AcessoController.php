<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class AcessoController extends Controller{
    public function login(){
        $title = "Login";
        return view('acesso.login')->with(compact('title'));
    }
    
    public function cadastro(){
        $title = "Cadastro";
        return view('acesso.registro')->with(compact('title'));
    }
    
    public function recupera(){
        $title = "Recuperar Senha";
        return view('acesso.recupera')->with(compact('title'));
    }

    public function verifica(Request $request){
        $usuario = DB::table('usuarios')->where('email', $request->usuario)->first();
        if($usuario != null){
            if($usuario->status == 1){
                if(password_verify($request->senha, $usuario->senha )){
                    $request->session()->flash('sucesso', 'Bom trabalho!');
                    $request->session()->put('id', $usuario->id);
                    $request->session()->put('nome', $usuario->nome);
                    $request->session()->put('tipo', $usuario->tipo);
                    $request->session()->put('logado', true);
                    DB::table('usuarios')->where('id', $usuario->id)->update(['tentativas' => 0]);
                    return redirect('/Dashboard');
                }else{
                    switch ($usuario->tentativas) {
                        case 0:
                            $request->session()->flash('alerta', 'Senha incorreta! Você tem mais 2 chances.');
                            DB::table('usuarios')
                                ->where('id', $usuario->id)
                                ->update(['tentativas' => 1]);
                            break;
                        case 1:
                            $request->session()->flash('alerta', 'Senha incorreta! Você tem mais 1 chances.');
                            DB::table('usuarios')
                                ->where('id', $usuario->id)
                                ->update(['tentativas' => 2]);
                            break;
                        case 2:
                            $request->session()->flash('erro', 'Senha incorreta! Usuário Bloqueado.');
                            $token = uniqid("BL", true);
                            DB::table('recuperacao')
                                ->insert([
                                    'token' => $token,
                                    'user_id' => $usuario->id,
                                    'criacao' => date('Y-m-d H:i:s')
                                ]);
                            DB::table('usuarios')
                                ->where('id', $usuario->id)
                                ->update([
                                    'tentativas' => 3,
                                    'status' => 0
                                ]);
                            Mail::send(new \App\Mail\desbloqueioUsuario($usuario, $token));
                            break;
                    }
                    return redirect()->back();
                }
            }else{
                $request->session()->flash('erro', 'Usuário Bloqueado!');
                return redirect()->back();
            }
        }else{
            $request->session()->flash('alerta', 'Usuário não encontrado.');
            return redirect()->back();
        }
    }

    public function cadastroAdd(Request $request){
        $verifica = DB::table('usuarios')->where('email', $request->email)->count();
        if ($verifica == 0) {
            DB::table('usuarios')->insert([
                'nome' => $request->nome,
                'senha' =>  password_hash($request->senha, PASSWORD_DEFAULT),
                'email' => $request->email,
                'whatsapp' => preg_replace("/[^0-9]/", "", $request->whatsapp),
                'criacao' => date('Y-m-d H:i:s')
            ]);
            Session::flash('sucesso', 'Cadastro Realizado.');
        }else{
            Session::flash('erro', 'Email já cadastrado em nosso sistema.');
        }
        return redirect('/Login');
    }
    
    public function recuperaAdd(Request $request){
        $usuario = DB::table('usuarios')->where('email', $request->email)->first();
        if($usuario != null){
            $token = uniqid("RC", true);
            DB::table('recuperacao')->insert([
                'token' => $token,
                'user_id' => $usuario->id,
                'criacao' => date('Y-m-d H:i:s')
            ]);
            Mail::send(new \App\Mail\RecuperacaoSenha($usuario, $token));
            $request->session()->flash('sucesso', 'Um email foi enviado com as instruções!');
            return redirect('/Login');
        }else{
            $request->session()->flash('erro', 'Usuário não encontrado.');
            return redirect()->back();
        }
    }

    public function verificaToken(Request $request, $token){
        $verifica = DB::table('recuperacao')->where('token', $token)->first();
        if ($verifica != null) {
            $date = new \DateTime($verifica->criacao);
            $now = new \DateTime();
            $intervalo = $date->diff($now)->format("%d");
            if ($intervalo == 0) {
                return view('acesso.senha')->with(compact('token'));
            }else{
                $request->session()->flash('alerta', 'Token fora da validade.');
                return redirect('/Login');
            }
        }else{
            $request->session()->flash('erro', 'Código Inválido');
            return redirect('/Login');
        }
    }

    public function novasenha(Request $request){
        $verifica = DB::table('recuperacao')->where('token', $request->token)->first();
        if ($verifica != null) {
            $date = new \DateTime($verifica->criacao);
            $now = new \DateTime();
            $intervalo = $date->diff($now)->format("%d");
            if ($intervalo == 0) {
                DB::table('usuarios')
                    ->where('id', $verifica->user_id)
                    ->update([
                        'senha' => password_hash($request->senha, PASSWORD_DEFAULT),
                        'tentativas' => 0,
                        'status' => 1
                        ]
                );
                DB::table('recuperacao')->where('token', $request->token)->delete();
                $request->session()->flash('sucesso', 'Faça o Login para continuar!');
                return redirect('/Login');
            }else{
                $request->session()->flash('atencao', 'Token fora da validade.');
                return redirect('/Login');
            }
        }else{
            $request->session()->flash('erro', 'Código Inválido');
            return redirect('/Login');
        }
    }

    public function logout(Request $request){
        session()->flush();
        $request->session()->flash('sucesso', 'Até Logo!');
        return redirect('/Login');
    }
}
