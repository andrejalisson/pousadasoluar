<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashBoardsController extends Controller{
    public function reservas(){
        $title = "DashBoard Financeiro";
        return view('dashboard.reservas')->with(compact( 'title'));
    }
    
}
