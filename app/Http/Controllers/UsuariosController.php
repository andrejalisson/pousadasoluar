<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsuariosController extends Controller{

    public function lista(){
        $title = "Usuários";
        return view('usuarios.lista')->with(compact( 'title'));
    }

    public function add(){
        $title = "Adicionar Usuários";
        return view('usuarios.add')->with(compact( 'title'));
    }

    public function editar($id){
        $usuario = DB::table('usuarios')->where('id', $id)->first();
        $title = "Editar Usuários";
        return view('usuarios.edt')->with(compact('title', 'usuario'));
    }

    public function addPost(Request $request){
        if ($request->status == null) {
            $status = 0;
        }else{
            $status = 1;
        }
        if ($request->senha1 == $request->senha2) {
            DB::table('usuarios')->insert([
                'nome' => $request->nome,
                'senha' => password_hash($request->senha1, PASSWORD_BCRYPT),
                'email' => $request->email,
                'whatsapp' => preg_replace("/[^0-9]/", "", $request->whatsapp),
                'criacao' => date('Y-m-d H:i:s'),
                'tentativas' => 0,
                'tipo' => $request->tipo,
                'status' => $status,
                ]
            );
            
            $request->session()->flash('sucesso', 'Usuário Adicionado com sucesso!');
            return redirect('/Usuarios');
        }else{
            $request->session()->flash('alerta', 'A senha não confere.');
            return redirect()->back();
        }
    }

    public function edtPost(Request $request, $id){
        if ($request->status == null) {
            $status = 0;
        }else{
            $status = 1;
        }
        if ($request->senha1 == "") {
            DB::table('usuarios')
                ->where('id', $id)
                ->update(['nome' => $request->nome,
                        'email' => $request->email,
                        'whatsapp' => preg_replace("/[^0-9]/", "", $request->whatsapp),
                        'tipo' => $request->tipo,
                        'status' => $status,
                ]);
            $request->session()->flash('sucesso', 'Usuário Editado com sucesso!');
            return redirect('/Usuarios');
        } else {
            if ($request->senha1 == $request->senha2) {
                DB::table('usuarios')
                    ->where('id', $id)
                    ->update([  'nome' => $request->nome,
                                'senha' => password_hash($request->senha1, PASSWORD_BCRYPT),
                                'email' => $request->email,
                                'whatsapp' => preg_replace("/[^0-9]/", "", $request->whatsapp),
                                'tipo' => $request->tipo,
                                'status' => $status,
                    ]);
                $request->session()->flash('sucesso', 'Usuário Editado com sucesso!');
                return redirect('/Usuarios');
            } else {
                $request->session()->flash('alerta', 'As senhas não confere.');
                return redirect()->back();
            }
        }
    }

    public function todasUsuarios(Request $request){

        $columns = array(
            0 =>'nome',
            1 =>'email',
            2 =>'whatsapp',
        );
        $totalData = DB::table('usuarios')->count();

        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $usuarios = DB::table('usuarios')
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
        }
        else{
            $search = $request->input('search.value');
            $usuarios =  DB::table('usuarios')
                            ->where('nome','LIKE',"%{$search}%")
                            ->orWhere('email','LIKE',"%{$search}%")
                            ->orWhere('whatsapp','LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
            $totalFiltered = DB::table('usuarios')
                            ->where('nome','LIKE',"%{$search}%")
                            ->orWhere('email','LIKE',"%{$search}%")
                            ->orWhere('whatsapp','LIKE',"%{$search}%")
                            ->count();
        }
        if(!empty($usuarios)){
            $data = array();
            foreach ($usuarios as $usuario){
                $nestedData['id'] = $usuario->id;
                $nestedData['nome'] = $usuario->nome;
                $nestedData['email'] = $usuario->email;
                $nestedData['whatsapp'] = "<a target=\"_blank\" href=\"https://api.whatsapp.com/send?phone=".$usuario->whatsapp."\" class=\"client-link text-success\">$usuario->whatsapp</a>";
                if ($usuario->tipo == 1) {
                    $tipoo = "<span class=\"badge badge-success\">Administrador</span>";
                }else if ($usuario->tipo == 2) {
                    $tipoo = "<span class=\"badge badge-warning\">Vendedor</span>";
                }else{
                    $tipoo = "<span class=\"badge badge-info\">Cliente</span>";
                }
                $nestedData['tipo'] = $tipoo;
                if ($usuario->status == 1){
                    $status = "<span class=\"badge badge-primary\">Ativo</span>";
                }else{
                    $status = "<span class=\"badge badge-danger\">Inativo</span>";
                }
                $nestedData['status'] = $status;
                $nestedData['opcoes'] = "   <a class=\"btn btn-warning btn-circle\" href=\"/Usuarios/Editar/".$usuario->id."\" type=\"button\"><i class=\"fas fa-pencil-alt\"\"></i></a>
                                            <a class=\"btn btn-danger btn-circle excluirusuario\" data-usuario=\"".$usuario->id."\" type=\"button\"><i class=\"fa fa-times excluirusuario data-usuario=\"".$usuario->id."\"\"></i></a>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        echo json_encode($json_data);
    }

    public function excluiUsuario(Request $request){
        DB::table('usuarios')->where('id', $request->id)->delete();
        return response()->json("success",200);
    }
}
