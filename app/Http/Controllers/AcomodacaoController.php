<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AcomodacaoController extends Controller{
    public function lista(){
        $title = "Acomodações";
        return view('acomodacoes.lista')->with(compact( 'title'));
    }

    public function add(){
        $title = "Adicionar Acomodação";
        return view('acomodacoes.add')->with(compact( 'title'));
    }

    public function addPost(Request $request){
        if ($request->status == null) {
            $status = 0;
        }else{
            $status = 1;
        }
        if ($request->site == null) {
            $site = 0;
        }else{
            $site = 1;
        }
        $valor = str_replace(['.',','],'', $request->valor);
        $id = DB::table('acomodacoes')->insertGetId([
            'titulo' => "$request->titulo",
            'valor' => number_format($valor,2,".",""),
            'atributos' => "$request->atributos",
            'site' => $site,
            'status' => $status,
            ]
        );

        DB::table('imagens')
                ->where('tipo', 2)
                ->where('chave', 0)
                ->update(['chave' => $id]);
        $request->session()->flash('sucesso', 'Acomodação adicionada com sucesso!');
        return redirect('/Acomodacoes');
    }


    public function imagens(Request $request){
        $image = $request->file('file');
        $imageName = uniqid('img').".".$image->extension();
        $image->move(public_path('images/acomocacoes'),$imageName);

        DB::table('imagens')->insert([
            'imagem' => $imageName,
            'tipo' => 2,
            'chave' => 0,
            ]
        );
        return response()->json(['success'=>$imageName]);
        
    }
}
