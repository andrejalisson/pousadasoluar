<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecuperacaoSenha extends Mailable
{
    use Queueable, SerializesModels;

    private $usuario;
    private $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\stdClass $usuario, $token){
        $this->usuario = $usuario;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        $this->subject("Recuperação de Senha");
        $this->to($this->usuario->email, $this->usuario->nome);
        return $this->view('Mails.RecuperacaoSenha', [
            'usuario' => $this->usuario,
            'token' => $this->token
        ]);
    }
}
