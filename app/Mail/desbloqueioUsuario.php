<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class desbloqueioUsuario extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\stdClass $usuario, $token){
        $this->usuario = $usuario;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        $this->subject("Bloqueio de conta");
        $this->to($this->usuario->email, $this->usuario->nome);
        return $this->view('Mails.bloqueioConta', [
            'usuario' => $this->usuario,
            'token' => $this->token
        ]);
    }
}
